import json
import matplotlib.pyplot as plt
import networkx as nx
from rdflib import Graph, URIRef, BNode, Literal, Namespace
from rdflib.namespace import FOAF
from rdflib.serializer import Serializer

new_g = nx.Graph()
g = Graph()
base = 'http://www.semanticweb.org/lingtelli/ontologies/2018/3/kangmei-healthcare-ontology-01#'
keys = ['hasExpression', 'informNegative', 'informPositive', 'informValue',
        'DecomposeInto', 'hasTaskValue', 'Next', 'First', 'hasChoice',
        'hasUserAction']
express = 'hasExpression'
negative = 'informNegative'
positive = 'informPositive'
inform_value = 'informValue'
decompose = 'DecomposeInto'
has_task = 'hasTaskValue'
the_next = 'Next'
the_first = 'First'
choice = 'hasChoice'
action = 'hasUserAction'
special_key = ['@id', '@type']
g.parse("/Users/lingtelli/Downloads/Kangmei_HealthCare_ontology.owl")

json_data = g.serialize(format='json-ld', indent=4)
dict_data = json.loads(json_data)
node_list = []

def print_spo(rdf_graph):
    for s, p, o in rdf_graph:
        print '-------------------'
        print 'subject: ', s
        print 'predicate: ', p
        print 'object: ', o
        print '--------------------'


def print_each_block(dict_data):
    for data in dict_data:
        print '-------------------START BLOCK-------------------' 
        print data
        print '-------------------END BLOCK-------------------'
        

def build_all_nodes(dict_data):
    for data in dict_data:
        if (base + express) in data:
            head = data.get('@id')
            head = head.replace(base, '')
            head = AnyNode(id=head)
            for key in keys:
                if (base+key) in data:
                    if key == express:
                        next_node = AnyNode(comment=data.get(base+key))
                    else:
                        next_node = data.get(base+key)[0].get('@id')
                        if next_node:
                            next_node = next_node.replace(base, '')
                        if not next_node:
                            next_node = key
                        next_node = AnyNode(next_node=next_node)
                    next_node.parent = head
            # print RenderTree(head)
            node_list.append(head)


def collect_nodes(dict_data):
    for data in dict_data:
        node_full = data.get('@id')
        # node = node_full.replace(base, '')
        node = node_full.split('#', 1)
        if len(node) > 1:
            new_g.add_node(node[1])
        else:
            new_g.add_node(node[0])


def build_edges(dict_data):
    for data in dict_data:
        base_node = None
        node_full = data.get('@id')
        node = node_full.split('#', 1)
        if len(node) > 1:
            base_node = node[1]
        else:
            base_node = node[0]
        for key in data:
            if key not in special_key:
                next_node = key.split('#', 1)
                if len(next_node) > 1:
                    new_g.add_edge(base_node, next_node[1])
                else:
                    new_g.add_edge(base_node, next_node[0])
    nx.draw(new_g)
    plt.show()


# print_each_block(dict_data)
collect_nodes(dict_data)
build_edges(dict_data)
